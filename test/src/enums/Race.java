package enums;

public enum Race {
    HUMAN, ELF, DWARF, ORC, DARKELF;
}
