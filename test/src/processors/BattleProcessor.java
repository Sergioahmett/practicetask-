package processors;

import entity.Creation;

public class BattleProcessor {
    private Creation creation1;
    private Creation creation2;
    private Creation tempCreation;
    private boolean flag = true;

    public BattleProcessor(Creation creation1, Creation creation2) {
        this.creation1 = creation1;
        this.creation2 = creation2;
    }

    public void startBattle() {
        if (creation1.getSpeed() >= creation2.getSpeed()) {
            System.out.println(creation1.getName() + " go first");
        }else{
            System.out.println(creation2.getName() + " go first");
            switchCreation();
        }
        while (flag) {
            attack();
            switchCreation();
        }
    }

    private void attack() {
        System.out.println(creation1.getName() + " deals " + creation2.getName() + " damage in size " + creation1.getpAttack() * ((100d - creation2.getpDefence()) / 100));
        creation2.setHp(creation2.getHp() - creation1.getpAttack() * ((100d - creation2.getpDefence()) / 100));
        if (creation2.isDead()) {
            flag = false;
            System.out.println(creation2.getName() + "is dead. " + creation1.getName() + " winner");
        } else {
            System.out.println(creation2.getName() + " still alive. It have " + creation2.getHp() + " hp.");
        }
    }

    private void switchCreation() {
        tempCreation = creation1;
        creation1 = creation2;
        creation2 = tempCreation;
    }
}
