package builders;

import entity.Creation;
import entity.UserCreation;

public abstract class CharacterBuilder {
    protected Creation character;

    public abstract CharacterBuilder createNewCharacter();
    public abstract Creation build();
    public abstract CharacterBuilder setCharacterParameters();
    public abstract CharacterBuilder setCharacterBag();
    public abstract CharacterBuilder setCharacterEquipment();
    public abstract CharacterBuilder setCharacterRewards();
}
