package builders;

import entity.Creation;
import entity.UserCreation;
import enums.Race;

public abstract class UserCharacterBuilder extends CharacterBuilder {
    protected String name;
    protected Race race;

    public UserCharacterBuilder(String name, Race race){
        this.name = name;
        this.race=race;
    }

    public CharacterBuilder createNewCharacter(){
        character = new UserCreation();
        return this;
    }
    public CharacterBuilder setCharacterRewards(){
        return this;
    }

    public abstract Creation build();
    public abstract CharacterBuilder setCharacterParameters();
    public abstract CharacterBuilder setCharacterBag();
    public abstract CharacterBuilder setCharacterEquipment();
}
