package builders;

import entity.Creation;
import entity.MonsterCreation;
import entity.UserCreation;
import enums.Race;

public abstract class MonsterCharacterBuilder extends CharacterBuilder {
    protected String name;
    protected Race race;

    public MonsterCharacterBuilder(String name, Race race){
        this.name = name;
        this.race=race;
    }

    public CharacterBuilder createNewCharacter(){
        character = new MonsterCreation();
        return this;
    }
    public CharacterBuilder setCharacterBag(){
        return this;
    }
    public CharacterBuilder setCharacterEquipment(){
        return this;
    }

    public abstract Creation build();
    public abstract CharacterBuilder setCharacterParameters();
    public abstract CharacterBuilder setCharacterRewards();
}
