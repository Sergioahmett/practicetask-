package builders;

import entity.Bag;
import entity.Creation;
import entity.UserCreation;
import entity.UserEquipment;
import enums.Class;
import enums.Race;

public class NewbieCharacterBuilder extends UserCharacterBuilder {


    public NewbieCharacterBuilder(String name, Race race) {
        super(name, race);
    }

    @Override
    public CharacterBuilder setCharacterParameters() {
        character.setId(0);
        character.setName(name);
        character.setLvl(1);
        character.setpAttack(10);
        character.setmAttack(1);
        character.setSpeed(100);
        character.setpDefence(5);
        character.setmDefence(2);
        character.setDodge(5);
        character.setBlockChance(0);
        character.setRace(race);
        character.setClazz(Class.NEWBIE);
        character.setHp(100);
        character.setMp(50);
        return this;
    }

    @Override
    public CharacterBuilder setCharacterBag() {
        ((UserCreation)character).setBag(new Bag());
        return this;
    }

    @Override
    public CharacterBuilder setCharacterEquipment() {
        ((UserCreation)character).setEquipment(new UserEquipment());
        return this;
    }

    @Override
    public Creation build() {
        return character;
    }
}
