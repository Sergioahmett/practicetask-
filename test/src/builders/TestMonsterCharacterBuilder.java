package builders;

import entity.*;
import enums.Class;
import enums.Race;

import java.util.HashMap;
import java.util.Map;

public class TestMonsterCharacterBuilder extends MonsterCharacterBuilder {


    public TestMonsterCharacterBuilder(String name, Race race) {
        super(name, race);
    }

    @Override
    public CharacterBuilder setCharacterParameters() {
        character.setId(0);
        character.setName(name);
        character.setLvl(1);
        character.setpAttack(8);
        character.setmAttack(1);
        character.setSpeed(110);
        character.setpDefence(20);
        character.setmDefence(2);
        character.setDodge(5);
        character.setBlockChance(0);
        character.setRace(race);
        character.setClazz(Class.NEWBIE);
        character.setHp(100);
        character.setMp(50);
        return this;
    }

    @Override
    public Creation build() {
        return character;
    }

    @Override
    public CharacterBuilder setCharacterRewards() {
        MonsterReward reward = new MonsterReward();
        Map<Integer, Equipment> rewardMap = new HashMap<>();
        rewardMap.put(10, new Equipment("1"));
        rewardMap.put(20, new Equipment("2"));
        rewardMap.put(30, new Equipment("3"));
        rewardMap.put(40, new Equipment("4"));
        rewardMap.put(50, new Equipment("5"));
        rewardMap.put(60, new Equipment("6"));
        rewardMap.put(70, new Equipment("7"));
        rewardMap.put(80, new Equipment("8"));
        rewardMap.put(90, new Equipment("9"));
        rewardMap.put(100, new Equipment("10"));
        reward.setRewardMap(rewardMap);
        ((MonsterCreation)character).setReward(reward);
        return this;
    }
}
