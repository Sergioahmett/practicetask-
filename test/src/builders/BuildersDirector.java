package builders;

import entity.Creation;
import entity.UserCreation;

public class BuildersDirector {
    private CharacterBuilder builder;

    public void setBuilder(CharacterBuilder builder){
        this.builder = builder;
    }

    public Creation getCharacter(){
        builder.createNewCharacter();
        builder.setCharacterParameters();
        builder.setCharacterBag();
        builder.setCharacterEquipment();
        builder.setCharacterRewards();
        return builder.build();
    }
}
