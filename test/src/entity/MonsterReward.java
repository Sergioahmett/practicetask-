package entity;

import java.util.*;

public class MonsterReward {
    private Map<Integer, Equipment> rewardMap = new HashMap<>();
    private int experience;
    private Random r = new Random();
    private int minRewardCount = 2;
    private int maxRewardCount = 5;

    public List getReward() {
        List<Equipment> result = new ArrayList<>(5);
        int counter = 0;
        do {
            for (Map.Entry<Integer, Equipment> entry : rewardMap.entrySet()) {
                if (entry.getKey() >= r.nextInt(100)) {
                    result.add(entry.getValue());
                    counter++;
                    if (counter == maxRewardCount) {
                        break;
                    }
                }
            }
        } while (counter < minRewardCount);
        return result;
    }

    public Map<Integer, Equipment> getRewardMap() {
        return rewardMap;
    }

    public void setRewardMap(Map<Integer, Equipment> rewardMap) {
        this.rewardMap = rewardMap;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    @Override
    public String toString() {
        return "MonsterReward{" +
                "rewardMap=" + rewardMap +
                ", experience=" + experience +
                ", r=" + r +
                ", minRewardCount=" + minRewardCount +
                ", maxRewardCount=" + maxRewardCount +
                '}';
    }
}
