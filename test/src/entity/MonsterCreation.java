package entity;

public class MonsterCreation extends Creation {
    private MonsterReward reward;

    public MonsterReward getReward() {
        return reward;
    }

    public void setReward(MonsterReward reward) {
        this.reward = reward;
    }

    @Override
    public String toString() {
        return "MonsterCreation{" +
                "reward=" + reward +
                ", race=" + race +
                ", clazz=" + clazz +
                ", hp=" + hp +
                ", mp=" + mp +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", lvl=" + lvl +
                ", pAttack=" + pAttack +
                ", mAttack=" + mAttack +
                ", speed=" + speed +
                ", pDefence=" + pDefence +
                ", mDefence=" + mDefence +
                ", dodge=" + dodge +
                ", blockChance=" + blockChance +
                '}';
    }
}
