package entity;

public class UserCreation extends Creation {
    private Bag bag;
    private UserEquipment equipment;
    private int experience;

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    public UserEquipment getEquipment() {
        return equipment;
    }

    public void setEquipment(UserEquipment equipment) {
        this.equipment = equipment;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    @Override
    public String toString() {
        return "UserCreation{" +
                "bag=" + bag +
                ", equipment=" + equipment +
                ", experience=" + experience +
                ", race=" + race +
                ", clazz=" + clazz +
                ", hp=" + hp +
                ", mp=" + mp +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", lvl=" + lvl +
                ", pAttack=" + pAttack +
                ", mAttack=" + mAttack +
                ", speed=" + speed +
                ", pDefence=" + pDefence +
                ", mDefence=" + mDefence +
                ", dodge=" + dodge +
                ", blockChance=" + blockChance +
                '}';
    }
}
