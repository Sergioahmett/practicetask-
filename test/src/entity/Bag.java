package entity;

import java.util.ArrayList;
import java.util.List;

public class Bag {
    private List<Equipment> equipments = new ArrayList<>(25);
    private int money = 0;
    private int size = 0;
    private int maxSize = 25;

    public boolean addToBag(Equipment equipment) {
        if (size < maxSize) {
            size++;
            return equipments.add(equipment);
        }
        return false;
    }

    public Equipment getFromBag(int itemIndex) {
        size--;
        return equipments.get(itemIndex);
    }

    public List<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(List equipments) {
        this.equipments = equipments;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public String toString() {
        return "Bag{" +
                "equipments=" + equipments +
                ", money=" + money +
                ", size=" + size +
                ", maxSize=" + maxSize +
                '}';
    }
}
