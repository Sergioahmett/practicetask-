package entity;

public class ObjectParameters {
    protected int id;
    protected String name;
    protected int lvl;
    protected int pAttack;
    protected int mAttack;
    protected int speed;
    protected int pDefence;
    protected int mDefence;
    protected int dodge;
    protected int blockChance;

    public int getBlockChance() {
        return blockChance;
    }

    public void setBlockChance(int blockChance) {
        this.blockChance = blockChance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getpAttack() {
        return pAttack;
    }

    public void setpAttack(int pAttack) {
        this.pAttack = pAttack;
    }

    public int getmAttack() {
        return mAttack;
    }

    public void setmAttack(int mAttack) {
        this.mAttack = mAttack;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getpDefence() {
        return pDefence;
    }

    public void setpDefence(int pDefence) {
        this.pDefence = pDefence;
    }

    public int getmDefence() {
        return mDefence;
    }

    public void setmDefence(int mDefence) {
        this.mDefence = mDefence;
    }

    public int getDodge() {
        return dodge;
    }

    public void setDodge(int dodge) {
        this.dodge = dodge;
    }

    @Override
    public String toString() {
        return "Params{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lvl=" + lvl +
                ", pAttack=" + pAttack +
                ", mAttack=" + mAttack +
                ", speed=" + speed +
                ", pDefence=" + pDefence +
                ", mDefence=" + mDefence +
                ", dodge=" + dodge +
                ", blockChance=" + blockChance +
                '}';
    }
}
