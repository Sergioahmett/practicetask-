package entity;

import enums.EquipmentType;

public class Equipment extends ObjectParameters{
    private EquipmentType type;
    private int price;

    public Equipment(String name) {
        this.name = name;
    }

    public EquipmentType getType() {
        return type;
    }

    public void setType(EquipmentType type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "type=" + type +
                ", price=" + price +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", lvl=" + lvl +
                ", pAttack=" + pAttack +
                ", mAttack=" + mAttack +
                ", speed=" + speed +
                ", pDefence=" + pDefence +
                ", mDefence=" + mDefence +
                ", dodge=" + dodge +
                ", blockChance=" + blockChance +
                '}';
    }
}
