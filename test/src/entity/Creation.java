package entity;

import enums.Class;
import enums.Race;

public abstract class Creation extends ObjectParameters {
    protected Race race;
    protected Class clazz;
    protected double hp;
    protected double mp;

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public Class getClazz() {
        return clazz;
    }
    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }
    public double getHp() {
        return hp;
    }

    public void setHp(double hp) {
        this.hp = hp;
    }

    public double getMp() {
        return mp;
    }

    public void setMp(double mp) {
        this.mp = mp;
    }

    public boolean isDead() {
        return hp <= 0;
    }
}
