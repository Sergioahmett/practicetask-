package entity;

public class UserEquipment {
    private Equipment head;
    private Equipment body;
    private Equipment arms;
    private Equipment pants;
    private Equipment legs;
    private Equipment lHand;
    private Equipment rHand;

    public Equipment getHead() {
        return head;
    }

    public void setHead(Equipment head) {
        this.head = head;
    }

    public Equipment getBody() {
        return body;
    }

    public void setBody(Equipment body) {
        this.body = body;
    }

    public Equipment getArms() {
        return arms;
    }

    public void setArms(Equipment arms) {
        this.arms = arms;
    }

    public Equipment getPants() {
        return pants;
    }

    public void setPants(Equipment pants) {
        this.pants = pants;
    }

    public Equipment getLegs() {
        return legs;
    }

    public void setLegs(Equipment legs) {
        this.legs = legs;
    }

    public Equipment getlHand() {
        return lHand;
    }

    public void setlHand(Equipment lHand) {
        this.lHand = lHand;
    }

    public Equipment getrHand() {
        return rHand;
    }

    public void setrHand(Equipment rHand) {
        this.rHand = rHand;
    }

    @Override
    public String toString() {
        return "UserEquipment{" +
                "head=" + head +
                ", body=" + body +
                ", arms=" + arms +
                ", pants=" + pants +
                ", legs=" + legs +
                ", lHand=" + lHand +
                ", rHand=" + rHand +
                '}';
    }
}
