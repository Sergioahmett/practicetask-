package test;

import builders.BuildersDirector;
import builders.NewbieCharacterBuilder;
import builders.TestMonsterCharacterBuilder;
import entity.MonsterCreation;
import entity.UserCreation;
import enums.Race;
import processors.BattleProcessor;

public class Main {
    public static void main(String[] args) {
        BuildersDirector d = new BuildersDirector();
        d.setBuilder(new NewbieCharacterBuilder("Warrior", Race.HUMAN));
        UserCreation ch = (UserCreation)d.getCharacter();
//        System.out.println(ch);
        d.setBuilder(new TestMonsterCharacterBuilder("Monster", Race.ORC));
        MonsterCreation mon = (MonsterCreation)d.getCharacter();
//        System.out.println(mon);
//        System.out.println(mon.getReward().getReward());
        new BattleProcessor(ch,mon).startBattle();
    }
}
